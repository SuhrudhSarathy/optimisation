#include "ceres/ceres.h"
#include "glog/logging.h"
#include <vector>

using ceres::CostFunction;
using ceres::AutoDiffCostFunction;
using ceres::Solver;
using ceres::Solve;
using ceres::Problem;
using ceres::CauchyLoss;

struct Circle
{
    Circle(double x, double y, double r) : x_(x), y_(y), r_(r) {}

    double x_;
    double y_;
    double r_;
};


struct PointFunctor
{
    PointFunctor(double t, double x) : t_(t), x_(x) {}

    template <typename T>
    bool operator() (const T* const A, T* residual) const
    {
        T x = A[0] + A[1] * t_ + A[2] * t_*t_ + A[3] * t_*t_*t_;
        residual[0] = (x - x_);

        return true;
    }

    private:
        const double t_;
        const double x_;
};

struct VelocityFunctor
{
    VelocityFunctor(double t, double v) : t_(t), v_(v) {}

    template <typename T>
    bool operator() (const T* const A, T* residual) const
    {
        T v = A[0] * 0.0 + A[1] * 1.0 + A[2] * 2 * t_ + A[3] * 3 *t_*t_;
        residual[0] = (v - v_);

        return true;
    }

    private:
        const double t_;
        const double v_;
};

struct PathLengthFunctor
{
    PathLengthFunctor(double t0, double tt) : t0_(t0), tt_(tt) {}

    template <typename T>
    bool operator() (const T* const A, const T* const B, T* residual) const
    {
        std::vector<double> t_vector;
        double dt = 0.01;
        double t = t0_;
        
        while (t <= tt_)
        {
            t_vector.push_back(t);
            t += dt;
        }

        residual[0] = T(0.0);

        for (int i=0; i < t_vector.size()-1; i++)
        {
            double ti = t_vector[i];
            double ti1 = t_vector[i+1];

            T xi = A[0] + A[1] * ti + A[2] * ti * ti + A[3] * ti * ti * ti;
            T xi1 = A[0] + A[1] * ti + A[2] * ti1*ti1 + A[3] * ti1*ti1*ti1;

            T yi = B[0] + B[1] * ti1 + B[2] * ti * ti + B[3] * ti * ti * ti;
            T yi1 = B[0] + B[1] * ti1 + B[2] * ti1*ti1 + B[3] * ti1*ti1*ti1;

            residual[0] += ((xi-xi1)*(xi-xi1) + (yi-yi1)*(yi-yi1))*0.1;
        }

        return true;
    }

    private:
    	const double t0_;
        const double tt_;
};

/* For now assume that there are circular obstacles placed on both sides and the trajectory has to avoid them*/
struct SafetyDistanceFunctor
{
    SafetyDistanceFunctor(double t0, double tt, double safety_r, std::vector<Circle>& circles) : t0_(t0), tt_(tt), safety_r_(safety_r), circles_(circles){}

    template <typename T>
    bool operator() (const T* const A, const T* const B, T* residual) const 
    {
        std::vector<double> t_vector;
        double dt = 0.01;
        double t = t0_;
        
        while (t <= tt_)
        {
            t_vector.push_back(t);
            t += dt;
        }

        residual[0] = T(0.0);
        
        for (auto& circle: circles_)
        {
            for (auto& t : t_vector)
            {
                T xi = A[0] + A[1] * t + A[2] * t * t + A[3] * t * t * t;
                T yi = B[0] + B[1] * t + B[2] * t * t + B[3] * t * t * t;

                T dx = xi - T(circle.x_);
                T dy = yi - T(circle.y_);

                T distance = sqrt(dx*dx+dy*dy) - T(circle.r_);

                if (distance < safety_r_)
                {
                    residual[0] += -distance;
                }
            }   
        }

        return true;

    }

    private:
        const double t0_;
        const double tt_;
        const double safety_r_;
        const std::vector<Circle> circles_;
};


int main(int argc, char** argv)
{
    std::vector<double> A = {0.1, 0.2, 0.3, 0.4};
    std::vector<double> B = {0.4, 0.3, 0.2, 0.1};

    double t0 = 0.0;
    double tt = 15.0;

    double x0 = 0.1;
    double xt = 5.0;

    double y0 = 0.1;
    double yt = 0.1;

    double vx0 = 0.5;
    double vxt = 0.5;

    double vy0 = 0.0;
    double vyt = 0.0;

    std::vector<Circle> circles = {Circle(2.5, 0, 0.05)};

    Problem problem;

    CostFunction* fx0 = new AutoDiffCostFunction<PointFunctor, 1, 4>(new PointFunctor(t0, x0));
    CostFunction* fxt = new AutoDiffCostFunction<PointFunctor, 1, 4>(new PointFunctor(tt, xt));

    CostFunction* fy0 = new AutoDiffCostFunction<PointFunctor, 1, 4>(new PointFunctor(t0, y0));
    CostFunction* fyt = new AutoDiffCostFunction<PointFunctor, 1, 4>(new PointFunctor(tt, yt));

    CostFunction* fvx0 = new AutoDiffCostFunction<PointFunctor, 1, 4>(new PointFunctor(t0, vx0));
    CostFunction* fvxt = new AutoDiffCostFunction<PointFunctor, 1, 4>(new PointFunctor(tt, vxt));

    CostFunction* fvy0 = new AutoDiffCostFunction<PointFunctor, 1, 4>(new PointFunctor(t0, vy0));
    CostFunction* fvyt = new AutoDiffCostFunction<PointFunctor, 1, 4>(new PointFunctor(tt, vyt));

    CostFunction* path_length = new AutoDiffCostFunction<PathLengthFunctor, 1, 4, 4>(new PathLengthFunctor(t0, tt));
    CostFunction* obstacle_cost = new AutoDiffCostFunction<SafetyDistanceFunctor, 1, 4, 4>(new SafetyDistanceFunctor(t0, tt, 0.25, circles));

    problem.AddResidualBlock(fx0, nullptr, A.data());
    problem.AddResidualBlock(fxt, nullptr, A.data());
    
    problem.AddResidualBlock(fvx0, nullptr, A.data());
    problem.AddResidualBlock(fvxt, nullptr, A.data());
    
    problem.AddResidualBlock(fy0, nullptr, B.data());
    problem.AddResidualBlock(fyt, nullptr, B.data());
    
    problem.AddResidualBlock(fvy0, nullptr, B.data());
    problem.AddResidualBlock(fvyt, nullptr, B.data());

    problem.AddResidualBlock(path_length, nullptr, A.data(), B.data());
    problem.AddResidualBlock(obstacle_cost, nullptr, A.data(), B.data());
    
    Solver::Options options;
    options.minimizer_progress_to_stdout = true;
    
    Solver::Summary summary;
    Solve(options, &problem, &summary);

    std::cout << summary.FullReport() << std::endl;

    std::cout << A[0] << ", " << A[1] << ", " << A[2] << ", " << A[3] << std::endl;
    std::cout << B[0] << ", " << B[1] << ", " << B[2] << ", " << B[3] << std::endl;
}