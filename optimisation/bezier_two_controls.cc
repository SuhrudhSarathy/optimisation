#include "ceres/ceres.h"
#include "glog/logging.h"
#include <vector>

using ceres::CostFunction;
using ceres::AutoDiffCostFunction;
using ceres::Solver;
using ceres::Solve;
using ceres::Problem;
using ceres::CauchyLoss;

struct Circle
{
    Circle(double x, double y, double r) : x_(x), y_(y), r_(r) {}

    double x_;
    double y_;
    double r_;
};

double get_nearest_distance(std::vector<Circle>& circles, double x, double y)
{
    double distance = 10000.0;
    for(auto& circle: circles)
    {
        double dx = x - circle.x_;
        double dy = y - circle.y_;

        double d = sqrt(dx * dx + dy * dy) - circle.r_;

        if (d < distance)
        {
            distance = d;
        }
    }
    
    return distance;
}

struct Point
{
    Point(double x, double y): x_(x), y_(y) {}

    double x_;
    double y_;
};


struct PathLengthFunctor
{
    PathLengthFunctor(Point x0, Point x1, double t0, double tt) : x0_(x0), x1_(x1), t0_(t0), tt_(tt) {}

    template <typename T>
    bool operator() (const T* const A, const T* const B, T* residual) const
    {
        std::vector<double> t_vector;
        double dt = 0.01;
        double t = t0_;
        while (t <= tt_)
        {
            t_vector.push_back(t);
            t += dt;
        }

        residual[0] = T(0.0);

        LOG(INFO) << "T size " << t_vector.size();

        for (int i=0; i < t_vector.size()-1; i++)
        {
            double ti = t_vector[i];
            double ti1 = t_vector[i+1];

            T xi = T(x0_.x_) * pow(1-ti, 3) + A[0] * T(3) * pow(1-ti, 2) * ti + A[1] * T(3) * (1-ti) * pow(ti, 2) + T(x1_.x_) * pow(ti, 3);
            T xi1 = T(x0_.x_) * pow(1-ti1, 3) + A[0] * T(3) * pow(1-ti1, 2) * ti1 + A[1] * T(3) * (1-ti1) * pow(ti1, 2) + T(x1_.x_) * pow(ti1, 3);;

            T yi = T(x0_.y_) * pow(1-ti, 3) + B[0] * T(3) * pow(1-ti, 2) * ti + B[1] * T(3) * (1-ti) * pow(ti, 2) + T(x1_.y_) * pow(ti, 3);
            T yi1 = T(x0_.y_) * pow(1-ti1, 3) + B[0] * T(3) * pow(1-ti1, 2) * ti1 + B[1] * T(3) * (1-ti1) * pow(ti1, 2) + T(x1_.y_) * pow(ti1, 3);;

            residual[0] += ((xi1-xi)*(xi1-xi) + (yi1-yi)*(yi1-yi))*0.1;
        }

        LOG(INFO) << "residual " << residual[0];

        return true;
    }

    private:
        const Point x0_;
        const Point x1_;
    	const double t0_;
        const double tt_;
};

struct AccelerationFunctor
{
    AccelerationFunctor(Point p0, Point p1, double t0, double tt) : p0_(p0), p1_(p1), t0_(t0), tt_(tt) {}

    template <typename T>
    bool operator() (const T* const A, const T* const B, T* residual) const
    {
        std::vector<double> t_vector;
        double dt = 0.01;
        double t = t0_;
        while (t <= tt_)
        {
            t_vector.push_back(t);
            t += dt;
        }

        residual[0] = T(0.0);

        for(auto& t: t_vector)
        {
            T ax = 6.0 * (1-t)*(A[1] - 2.0*A[0] + T(p0_.x_)) + 6.0*t*(T(p1_.x_) - 2.0*A[1] + A[0]);
            T ay = 6.0 * (1-t)*(B[1] - 2.0*B[0] + T(p0_.y_)) + 6.0*t*(T(p1_.y_) - 2.0*B[1] + B[0]);

            residual[0] += (ax*ax + ay*ay);
        }

        LOG(INFO) << "residual " << residual[0];

        return true;
    }

    private:
        const Point p0_;
        const Point p1_;
    	const double t0_;
        const double tt_;
};

struct SafetyDistanceFunctor
{
    SafetyDistanceFunctor(Point p0, Point p1, double t0, double tt, double safety_r, std::vector<Circle>& circles) : p0_(p0), p1_(p1), t0_(t0), tt_(tt), safety_r_(safety_r), circles_(circles){}

    template <typename T>
    bool operator() (const T* const A, const T* const B, T* residual) const 
    {
        std::vector<double> t_vector;
        double dt = 0.01;
        double t = t0_;
        
        while (t <= tt_)
        {
            t_vector.push_back(t);
            t += dt;
        }

        residual[0] = T(0.0);
        
        
        for (auto& t : t_vector)
        {
            T xi = T(p0_.x_) * pow(1-t, 3) + A[0] * 3.0 * pow(1-t, 2) * t + A[1] * 3.0 * (1-t) * pow(t, 2) + T(p1_.x_) * pow(t, 3);
            T yi = T(p0_.y_) * pow(1-t, 3) + B[0] * 3.0 * pow(1-t, 2) * t + B[1] * 3.0 * (1-t) * pow(t, 2) + T(p1_.y_) * pow(t, 3);

            T distance = T(10000.0);
            for(auto& circle: circles_)
            {
                T dx = xi - T(circle.x_);
                T dy = yi - T(circle.y_);

                T d = sqrt(dx * dx + dy * dy) - T(circle.r_);

                if (d < distance)
                {
                    distance = d;
                }
            }

            // Cost function
            if (distance < 0.0)
            {
                residual[0] += -distance + T(safety_r_ * 0.5);
            }

            else if (0 < distance < T(safety_r_))
            {
                residual[0] += pow(distance - T(safety_r_), 2.0) * 1.0 / T(2.0*safety_r_);
            }
        }

        return true;

    }

    private:
        const Point p0_;
        const Point p1_;
        const double t0_;
        const double tt_;
        const double safety_r_;
        const std::vector<Circle> circles_;
};

int main(int argc, char** argv)
{
    google::SetLogDestination(0, "./info.log");
    google::InitGoogleLogging("log");
    FLAGS_timestamp_in_logfile_name = false;
    LOG(INFO) << "initialised logging"; 

    std::vector<double> A = {0, 5.6};
    std::vector<double> B = {2.0, 4.4};

    // Add initial Points for X and Y
    Point p0(1.2, 1.2);
    Point p1(9.8, 1.2);

    // Constraints for obstacles
    std::vector<Circle> circles = {Circle(3.5, -1.5, 0.25), Circle(5.9, -1.5, 0.25)};

    Problem problem;

    CostFunction* path_length = new AutoDiffCostFunction<PathLengthFunctor, 1, 2, 2>(new PathLengthFunctor(p0, p1, 0, 1));
    problem.AddResidualBlock(path_length, nullptr, A.data(), B.data());

    // CostFunction* acceleration = new AutoDiffCostFunction<AccelerationFunctor,1, 2, 2>(new AccelerationFunctor(p0, p1, 0, 1));
    // problem.AddResidualBlock(acceleration, nullptr, A.data(), B.data());

    CostFunction* obstacle_cost = new AutoDiffCostFunction<SafetyDistanceFunctor, 1, 2, 2>(new SafetyDistanceFunctor(p0, p1, 0, 1, 0.5, circles));
    problem.AddResidualBlock(obstacle_cost, nullptr, A.data(), B.data());

    Solver::Options options;
    options.linear_solver_type = ceres::LinearSolverType::DENSE_QR;
    options.minimizer_progress_to_stdout = false;
    
    Solver::Summary summary;
    Solve(options, &problem, &summary);

    // std::cout << summary.FullReport() << std::endl;

    std::cout << p0.x_ << ", " << A[0] << ", " << A[1] << ", " << p1.x_ << std::endl;
    std::cout << p0.y_ << ", " << B[0] << ", " << B[1] << ", " << p1.y_ << std::endl;
}