#include "ceres/ceres.h"
#include "glog/logging.h"
#include <vector>

using ceres::CostFunction;
using ceres::AutoDiffCostFunction;
using ceres::Solver;
using ceres::Solve;
using ceres::Problem;
using ceres::CauchyLoss;

struct Circle
{
    Circle(double x, double y, double r) : x_(x), y_(y), r_(r) {}

    double x_;
    double y_;
    double r_;
};

struct PointFunctor
{
    PointFunctor(double t, double x) : t_(t), x_(x) {}

    template <typename T>
    bool operator() (const T* const A, T* residual) const
    {
        T x = A[0] * pow(1-t_, 3) + A[1] * 3.0 * pow(1-t_, 2) * t_ + A[2] * 3.0 * (1-t_) * pow(t_, 2) + A[3] * pow(t_, 3);
        residual[0] = (x_ - x);

        return true;
    }

    private:
        const double t_;
        const double x_;
};

struct PathLengthFunctor
{
    PathLengthFunctor(double t0, double tt) : t0_(t0), tt_(tt) {}

    template <typename T>
    bool operator() (const T* const A, const T* const B, T* residual) const
    {
        std::vector<double> t_vector;
        double dt = 0.01;
        double t = t0_;
        while (t <= tt_)
        {
            t_vector.push_back(t);
            t += dt;
        }

        residual[0] = T(0.0);

        LOG(INFO) << "T size " << t_vector.size();

        for (int i=0; i < t_vector.size()-1; i++)
        {
            double ti = t_vector[i];
            double ti1 = t_vector[i+1];

            T xi = A[0] * pow(1-ti, 3) + A[1] * T(3) * pow(1-ti, 2) * ti + A[2] * T(3) * (1-ti) * pow(ti, 2) + A[3] * pow(ti, 3);
            T xi1 = A[0] * pow(1-ti1, 3) + A[1] * T(3) * pow(1-ti1, 2) * ti1 + A[2] * T(3) * (1-ti1) * pow(ti1, 2) + A[3] * pow(ti1, 3);;

            T yi = B[0] * pow(1-ti, 3) + B[1] * T(3) * pow(1-ti, 2) * ti + B[2] * T(3) * (1-ti) * pow(ti, 2) + B[3] * pow(ti, 3);
            T yi1 = B[0] * pow(1-ti1, 3) + B[1] * T(3) * pow(1-ti1, 2) * ti1 + B[2] * T(3) * (1-ti1) * pow(ti1, 2) + B[3] * pow(ti1, 3);;

            residual[0] += ((xi1-xi)*(xi1-xi) + (yi1-yi)*(yi1-yi))*0.1;
        }

        LOG(INFO) << "residual " << residual[0];

        return true;
    }

    private:
    	const double t0_;
        const double tt_;
};

struct SafetyDistanceFunctor
{
    SafetyDistanceFunctor(double t0, double tt, double safety_r, std::vector<Circle>& circles) : t0_(t0), tt_(tt), safety_r_(safety_r), circles_(circles){}

    template <typename T>
    bool operator() (const T* const A, const T* const B, T* residual) const 
    {
        std::vector<double> t_vector;
        double dt = 0.01;
        double t = t0_;
        while (t <= tt_)
        {
            t_vector.push_back(t);
            t += dt;
        }

        residual[0] = T(0.0);
        
        for (auto& t : t_vector)
        {
            T xi = A[0] * pow(1-t, 3) + A[1] * 3.0 * pow(1-t, 2) * t + A[2] * 3.0 * (1-t) * pow(t, 2) + A[3] * pow(t, 3);
            T yi = B[0] * pow(1-t, 3) + B[1] * 3.0 * pow(1-t, 2) * t + B[2] * 3.0 * (1-t) * pow(t, 2) + B[3] * pow(t, 3);

            T distance = T(10000.0);
            for(auto& circle: circles_)
            {
                T dx = xi - T(circle.x_);
                T dy = yi - T(circle.y_);

                T d = sqrt(dx * dx + dy * dy) - T(circle.r_);

                if (d < distance)
                {
                    distance = d;
                }
            }

            // LOG(INFO) << 10.0*1.0/distance;

            // Cost function
            if (distance < 0.0)
            {
                residual[0] += -distance + T(safety_r_ * 0.5);
            }

            else if (0 < distance < T(safety_r_))
            {
                residual[0] += pow(distance - T(safety_r_), 2.0) * 1.0 / T(2.0*safety_r_);
            }
        }   


        return true;

    }

    private:
        const double t0_;
        const double tt_;
        const double safety_r_;
        const std::vector<Circle> circles_;
};

int main(int argc, char** argv)
{
    google::SetLogDestination(0, "./bezeir_info.log");
    google::InitGoogleLogging("log");
    FLAGS_timestamp_in_logfile_name = false;

    std::vector<double> A = {0.0, 1.0, 2.0, 3.0};
    std::vector<double> B = {0.0, 1.0, 2.0, 3.0};

    // Add constraints for the endpoints in x and y
    double x0 = 1.5;
    double y0 = 1.5;

    double x1 = 10.0;
    double y1 = 1.5;

    // Constraints for obstacles
    std::vector<Circle> circles = {Circle(2.5, 1.5, 0.25), Circle(5.0, 1.5, 0.5)};

    Problem problem;

    CostFunction* xinit = new AutoDiffCostFunction<PointFunctor, 1, 4>(new PointFunctor(0, x0));
    problem.AddResidualBlock(xinit, nullptr, A.data());
    CostFunction* yinit = new AutoDiffCostFunction<PointFunctor, 1, 4>(new PointFunctor(0, y0));
    problem.AddResidualBlock(yinit, nullptr, B.data());

    CostFunction* xend = new AutoDiffCostFunction<PointFunctor, 1, 4>(new PointFunctor(1, x1));
    problem.AddResidualBlock(xend, nullptr, A.data());
    CostFunction* yend = new AutoDiffCostFunction<PointFunctor, 1, 4>(new PointFunctor(1, y1));
    problem.AddResidualBlock(yend, nullptr, B.data());

    // CostFunction* path_length = new AutoDiffCostFunction<PathLengthFunctor, 1, 4, 4>(new PathLengthFunctor(0, 1));
    // problem.AddResidualBlock(path_length, nullptr, A.data(), B.data());

    CostFunction* obstacle_cost = new AutoDiffCostFunction<SafetyDistanceFunctor, 1, 4, 4>(new SafetyDistanceFunctor(0, 1, 0.25, circles));
    problem.AddResidualBlock(obstacle_cost, nullptr, A.data(), B.data());

    Solver::Options options;
    options.minimizer_progress_to_stdout = true;
    
    Solver::Summary summary;
    Solve(options, &problem, &summary);

    std::cout << summary.FullReport() << std::endl;

    std::cout << A[0] << ", " << A[1] << ", " << A[2] << ", " << A[3] << std::endl;
    std::cout << B[0] << ", " << B[1] << ", " << B[2] << ", " << B[3] << std::endl;
}