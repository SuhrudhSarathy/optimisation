# Collision free Path optimisation
Using Ceres Optimisier to solve for collision free path using various constraints.

# Prerequisites
1. Install Ceres and Prereqs from the [official website](http://ceres-solver.org/installation.html)

# Installation of this repo
```bash
mkdir build
cmake ..
make
```
This will `build` all the files.

## Expts
1. To run the bezier expts, after building the files, run `bezier_two` file.
2. Put the printed values in the lists `A` and `B` respectively in the file `plot_bezeir.py` to visualise the results.