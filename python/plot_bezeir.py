import matplotlib.pyplot as plt
import numpy as np

T = np.linspace(0, 1, 100)


A = [1.2, 4.12313, 6.6887, 9.8]
B = [1.2, 1.19789, 1.19823, 1.2]

A_i = [1.2, 0, 5.6, 9.8]
B_i = [1.2, 2.0, 4.4, 1.2]

circles = [
    [[3.5, 0], 0.25],
    [[5.9, 0], 0.5]
]

if __name__ == "__main__":

    fig, ax = plt.subplots(1, 1)

    X = []
    Y = []

    for t in T:
        X.append(A[0]*(1-t)**3 +A[1]*3*t*(1-t)**2 + A[2]*3*(1-t)*t**2 + A[3]*t**3)
        Y.append(B[0]*(1-t)**3 +B[1]*3*t*(1-t)**2 + B[2]*3*(1-t)*t**2 + B[3]*t**3)

    # Plot the circles now
    for c in circles:
        print(c)
        ax.add_artist(plt.Circle(c[0], c[1], fill=True, color="black", alpha=0.75))

    # Plot control points
    ax.set_ylim([-2, 5])
    ax.scatter(A, B, marker="+", color="red", label="Final Control points")

    ax.scatter(A_i, B_i, marker="*", color="green", s=50, label="Inital Control points")

    plt.suptitle("Trajectory Optimisation")
    ax.set_xlabel("X(m)")
    ax.set_ylabel("Y(m)")

    ax.plot(X, Y)
    plt.legend()
    plt.show()