import numpy as np
import matplotlib.pyplot as plt

T0 = 0
TT = 15.0

x0 = 0.1
xt = 5.0

y0 = 0.1
yt = 0.1

vx0 = 0.5
vxt = 0.5

vy0 = 0.0
vyt = 0.0

circles = [
    [[2.5, 0], 0.05]
]

A = [0.291966, 3.73495, -1.59204, 0.0902412]
B = [0.0571593, 0.807039, -0.352646, 0.0199166]

# here the plotting takes place
if __name__ == "__main__":
    def getVal(A, t):
        return A[0] + A[1]*t + A[2]*t*t + A[3]*t*t*t
    

    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    t = np.linspace(T0, TT, 100)
    
    X = [getVal(A, t_) for t_ in t]
    Y = [getVal(B, t_) for t_ in t]

    # Plot the circles now
    for c in circles:
        ax.add_artist(plt.Circle(c[0], c[1], fill=False, color="black"))

    # ax.set_xlim(-1, 2)
    # ax.set_ylim(-1, 2)
    ax.plot(X, Y, color="green")

    plt.show()