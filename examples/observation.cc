#include "ceres/ceres.h"
#include "glog/logging.h"
#include <cmath>

using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solve;
using ceres::Solver;
using std::cos;
using std::sin;
using std::pow;

struct ObservationFunctor
{
    ObservationFunctor(double x_obs, double y_obs, double r, double cos, double sin) : x_obs_(x_obs), y_obs_(y_obs), r_(r), cos_(cos), sin_(sin) {}

    template <typename T>
    bool operator()(const T* const x, const T* const y, T* residual) const 
    {
        T x_pred = x[0] + r_ * cos_;
        T y_pred = y[0] + r_ * sin_;

        residual[0] = pow(x_obs_ - x_pred, 2) + pow(y_obs_ - y_pred, 2);

        return true;
    }

    private:
    	const double x_obs_;
        const double y_obs_;
        const double r_;
        const double cos_;
        const double sin_;
};

int main(int argc, char** argv)
{
    const double r = std::sqrt(2);
    Problem problem;

    double x = 0.75;
    double y = -2;

    CostFunction* f1 = new AutoDiffCostFunction<ObservationFunctor, 1, 1, 1>(new ObservationFunctor(5.0, 5.0, r, cos(M_PI_4), sin(M_PI_4)));
    CostFunction* f2 = new AutoDiffCostFunction<ObservationFunctor, 1, 1, 1>(new ObservationFunctor(-5.0, 5.0, r, cos(3.0*M_PI_4), sin(3.0*M_PI_4)));
    CostFunction* f3 = new AutoDiffCostFunction<ObservationFunctor, 1, 1, 1>(new ObservationFunctor(-5.0, -5.0, r, cos(5*M_PI_4), sin(5*M_PI_4)));
    CostFunction* f4 = new AutoDiffCostFunction<ObservationFunctor, 1, 1, 1>(new ObservationFunctor(5.0, -5.0, r, cos(7*M_PI_4), sin(7*M_PI_4)));

    problem.AddResidualBlock(f1, nullptr, &x, &y);
    problem.AddResidualBlock(f2, nullptr, &x, &y);
    problem.AddResidualBlock(f3, nullptr, &x, &y);
    problem.AddResidualBlock(f4, nullptr, &x, &y);

    Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;
    Solver::Summary summary;
    Solve(options, &problem, &summary);

    std::cout << summary.FullReport() << std::endl;

    std::cout << "X: " << x << " Y: " << y << std::endl;
    

    return 0;
}